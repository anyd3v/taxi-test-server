package com.test.taxy;

import java.net.*;
import java.io.*;

public class Server {

    private static final int PORT = 6666;

    private static final String REGIONS = "[{\"id\":1,\"name\":\"Region 1\"},{\"id\":2,\"name\":\"Region 2\"},{\"id\":3,\"name\":\"Region 3\"},{\"id\":4,\"name\":\"Region 4\"},{\"id\":5,\"name\":\"Region 5\"}]";

    public static void main(String[] args) {

        try {
            final ServerSocket ss = new ServerSocket(PORT);

            final Socket socket = ss.accept();

            final InputStream sin = socket.getInputStream();
            final OutputStream sout = socket.getOutputStream();

            final DataInputStream in = new DataInputStream(sin);
            final DataOutputStream out = new DataOutputStream(sout);

            while(true) {
                final String line = in.readUTF();
                String result;
                switch (line) {
                    case "regions":
                        result = REGIONS;
                        break;
                    default:
                        result = "unknown command";
                        break;
                }
                System.out.println(result);
                out.writeUTF(result);
                out.flush();
            }
        }
        catch(Exception x) {
            x.printStackTrace();
        }
    }

}
